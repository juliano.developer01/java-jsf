package br.com.java_jsf.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.java_jsf.util.JPAUtil;

public class DaoGeneric<E> {

	public void save(E entidade) {
		EntityManager entityManager = JPAUtil.getEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		
		entityManager.persist(entidade);
		
		entityTransaction.commit();
		entityManager.close();
	}
	
	
	@SuppressWarnings("unchecked")
    public List<E> findAll(E entidade) {
		EntityManager entityManager = JPAUtil.getEntityManager();
      return entityManager.createQuery("FROM " +
      entidade.getClass().getName()).getResultList();
    }
	
	public void merge(E entidade) {
		EntityManager entityManager = JPAUtil.getEntityManager();
        try {
           entityManager.getTransaction().begin();
           entityManager.merge(entidade);
           entityManager.getTransaction().commit();
        } catch (Exception ex) {
           ex.printStackTrace();
           entityManager.getTransaction().rollback();
        }
      }
	
	public void delete(E entidade) {
		EntityManager entityManager = JPAUtil.getEntityManager();
        try {
           entityManager.getTransaction().begin();
           entityManager.remove(entityManager.contains(entidade) ? entidade : entityManager.merge(entidade));
           entityManager.getTransaction().commit();
        } catch (Exception ex) {
           ex.printStackTrace();
           entityManager.getTransaction().rollback();
        }
      }
	

}
