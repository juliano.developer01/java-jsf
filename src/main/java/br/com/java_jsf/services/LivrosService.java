package br.com.java_jsf.services;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.java_jsf.dao.DaoGeneric;
import br.com.java_jsf.domain.Livros;

@ManagedBean(name = "LivrosService")
@ApplicationScoped
public class LivrosService {
	
	private Livros livro = new Livros();
	private DaoGeneric<Livros> daoGeneric = new DaoGeneric<Livros>();
	
	public List<Livros> buscar(){
		List<Livros> busca= new ArrayList<Livros>();
		List<Livros> list= daoGeneric.findAll(livro);
		list.forEach( item -> busca.add(item));
		
		return busca;
	}

}
