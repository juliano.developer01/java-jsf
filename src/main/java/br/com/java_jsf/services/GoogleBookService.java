package br.com.java_jsf.services;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.com.java_jsf.domain.GoogleBook;

@ManagedBean(name = "GoogleBookService")
@ApplicationScoped
public class GoogleBookService {
	
	private List<GoogleBook> books = new ArrayList<GoogleBook>();
	
	public List<GoogleBook> createBooks(List<String> id, List<String> title, List<String> description, List<String> image) {
        
        for(int i = 0 ; i <title.size()-1 ; i++) {
        	books.add(new GoogleBook(id.get(i), title.get(i), description.get(i), image.get(i)));
        }
         
        return books;
    }
	
}
