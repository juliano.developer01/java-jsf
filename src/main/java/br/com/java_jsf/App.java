package br.com.java_jsf;

import javax.persistence.Persistence;

public class App {
	
	public static void main( String[] args ){
    	try{
    		Persistence.createEntityManagerFactory("java-jsf");
    	}catch(Exception e) {
    		System.out.println(e.getMessage());
    	}
    }	
}
