package br.com.java_jsf.domain;

import java.io.Serializable;

public class GoogleBook implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String titulo;
	private String descricao;
	private String foto;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public GoogleBook() {
		
	}
	
	public GoogleBook(String id, String titulo, String descricao, String foto) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descricao = descricao;
		this.foto = foto;
	}
	
}
