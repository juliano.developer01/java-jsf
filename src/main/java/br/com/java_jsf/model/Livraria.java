package br.com.java_jsf.model;

import java.util.List;

import com.google.gson.JsonObject;


public class Livraria {
	
	public String kind;
    public String totalItens;
    public List<Volumes> items;
    
    public class Volumes {
    	public String id;
    	public JsonObject volumeInfo;
    	
    }
   
}
