package br.com.java_jsf.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

import br.com.java_jsf.dao.DaoGeneric;
import br.com.java_jsf.domain.Livros;
import br.com.java_jsf.services.LivrosService;
import br.com.java_jsf.util.JPAUtil;

@ManagedBean(name = "livrosView")
@ViewScoped
public class LivrosView  implements Serializable {
	private static final long serialVersionUID = 1L;
     
    private List<Livros> livros;
    
    private Livros selectedBook;
    
	
	private Livros livro = new Livros();
	private DaoGeneric<Livros> daoGeneric = new DaoGeneric<Livros>();
	
	
	@ManagedProperty("#{LivrosService}")
    private LivrosService service;
	
	@PostConstruct
    public void init() {
		JPAUtil em = new JPAUtil();
		em.getEntityManager();
		
		livros = service.buscar();
    }
	
	public void limpar(){
		livros.clear();
	}
	
	public List<Livros> getLivros() {
        return livros;
    }
	
	public LivrosService getService() {
		return service;
	}

	public void setService(LivrosService service) {
		this.service = service;
	}

	public void setLivros(List<Livros> livros) {
		this.livros = livros;
	}

	public Livros getLivro() {
		return livro;
	}

	public void setLivro(Livros livro) {
		this.livro = livro;
	}
	
	public String atualizar() {
		daoGeneric.merge(livro);
		return "Registro Atualizado com sucesso!";
	}
	
	public List<Livros> buscar(){
		List<Livros> busca= new ArrayList<Livros>();
		List<Livros> list= daoGeneric.findAll(livro);
		list.forEach( item -> busca.add(item));
		
		return busca;
	}
	
	public Livros getSelectedBook() {
        return selectedBook;
    }
 
    public void setSelectedBook(Livros selectedBook) {
        this.selectedBook = selectedBook;
    }
    
    public String deletar() {
    	daoGeneric.delete(getSelectedBook());
    	FacesMessage msg = new FacesMessage("Deletado com sucesso o livro ", getSelectedBook().getTitulo());
    	FacesContext.getCurrentInstance().addMessage(null, msg);
    	livros = service.buscar();
		return "Registro deletado com sucesso!";
	}
	
	public void onRowEdit(RowEditEvent<Livros> event) {
		livro.setId(event.getObject().getId());
		livro.setTitulo(event.getObject().getTitulo());
		livro.setDescricao(event.getObject().getDescricao());
		livro.setFoto(event.getObject().getFoto());
        daoGeneric.merge(livro);
        
        FacesMessage msg = new FacesMessage("informações editada do livro ", event.getObject().getTitulo());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onRowCancel(RowEditEvent<Livros> event) {
        FacesMessage msg = new FacesMessage("Edição cancelada", event.getObject().getTitulo());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onCellEdit(CellEditEvent<?> event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
         
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Célula Alterada", "Antes: " + oldValue + ", Depois:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
	
	
}
