package br.com.java_jsf.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.google.gson.Gson;

import br.com.java_jsf.dao.DaoGeneric;
import br.com.java_jsf.domain.GoogleBook;
import br.com.java_jsf.domain.Livros;
import br.com.java_jsf.model.Imagens;
import br.com.java_jsf.model.Livraria;
import br.com.java_jsf.model.Livro;
import br.com.java_jsf.services.GoogleBookService;
@ManagedBean(name = "googleBookView")
@ViewScoped
public class GoogleBookView implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String nome;
	
    private List<String> id  = new ArrayList<String>();
	private List<String> title  = new ArrayList<String>();
	private List<String> description = new ArrayList<String>();
	private List<String> image = new ArrayList<String>();
	
	private List<GoogleBook> books;
	
	private DaoGeneric<Livros> daoGeneric = new DaoGeneric<Livros>();
	
	private GoogleBook selectedBook;
	
	private Livros livro = new Livros();
	
	@ManagedProperty("#{GoogleBookService}")
    private GoogleBookService service;
	

	public String pesquisar() {
		//Aqui faço a limkpeza dos Arrays para uma nova pesquisa
		if(id.size()>0){
			books.clear();
		}
		title.clear();
		description.clear();
		image.clear();
		id.clear();
		
		//Aqui Faço uma nova pesquisa
		try {
			//Fazendo a requisição Http
			URL url = new URL("https://www.googleapis.com/books/v1/volumes?q="+getNome().replace( " ", "+" ));
			
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			
			if (con.getResponseCode() != 200) {
				FacesMessage msg = new FacesMessage("Sua consulta não retornou resultado ", "HTTP error code : "+ con.getResponseCode());
		    	FacesContext.getCurrentInstance().addMessage(null, msg);
			}
	
			//Lendo o Response
			
			BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream()), "UTF-8"));
			
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			
			Gson gson = new Gson();
			
			Livraria result = gson.fromJson(sb.toString(), Livraria.class);
			
			// passando o Json para os Arrays
			try {
			for (Livraria.Volumes volumes : result.items) {
				
				this.id.add(volumes.id);
				Livro vol = gson.fromJson(volumes.volumeInfo, Livro.class);
				this.title.add(vol.title);
				this.description.add(vol.description);
				Imagens img = gson.fromJson(vol.imageLinks, Imagens.class);
				this.image.add(img.thumbnail);
				
			}
			}catch(Exception e) {
				FacesMessage msg = new FacesMessage("Houve algo errado com sua consulta ", "Tente melhorar a palavra chave");
		    	FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			con.disconnect();
	
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		books=service.createBooks(id,  title, description, image);
		
		return "";
	}

	
	public String atualizar() {
		//Selecionado os dados do livro a ser salvo
		livro.setId(selectedBook.getId());
		livro.setTitulo(selectedBook.getTitulo());
		livro.setDescricao(selectedBook.getDescricao());
		livro.setFoto(selectedBook.getFoto());
		//Salvando em nossa tabela
		daoGeneric.merge(livro);
		FacesMessage msg = new FacesMessage("Favoritado com sucesso o livro ", getSelectedBook().getTitulo());
    	FacesContext.getCurrentInstance().addMessage(null, msg);
		return "Registro Atualizado com sucesso!";
	}
	
	public List<GoogleBook> getBooks() {
		return books;
	}

	public void setBooks(List<GoogleBook> books) {
		this.books = books;
	}
	
		
		public GoogleBookService getService() {
		return service;
	}

	public void setService(GoogleBookService service) {
		this.service = service;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

	public Livros getLivro() {
		return livro;
	}

	public void setLivro(Livros livro) {
		this.livro = livro;
	}

	public GoogleBook getSelectedBook() {
		return selectedBook;
	}

	public void setSelectedBook(GoogleBook selectedBook) {
		this.selectedBook = selectedBook;
	}
	
	

	
	
	
}
